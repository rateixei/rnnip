# RNNIP

Repo for the code for training and evaluating the [flavor tagging RNN](https://cds.cern.ch/record/2255226/files/ATL-PHYS-PUB-2017-003.pdf).

## Step 1: Software setup

The following steps show how to set up conda enviornment which works well for the data processing and plotting scripts.

```
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
sh Miniconda3-latest-Linux-x86_64.sh
conda create --name=py3 python=3 mkl jupyter numpy scipy matplotlib scikit-learn h5py theano tensorflow keras pandas tqdm uproot xarray
pip install uproot
pip install scikit-hep
```

This enviornment can also be used to train models, although its faster to 
train on gpus for larger jobs.


## Step 2: Data preprocessing

The preprocessing happens in the `root_to_np.py` function, which can outputs Keras ready training and test files.

To see what functionality is available, run

`python root_to_np.py --help`

### Input files options to start from

There are a few different starting places you have when running the file.

1. From Ntuples processed from the [flavor tagging performance framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/FlavourTagPerformanceFramework):

`python root_to_np.py --nJets 1000 --jetCollection PFLow --filename /afs/cern.ch/user/h/hartman/Work/public/FTAG/performance-Ntuples/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3126_r10201_p3703/DAOD_FTAG1.16199992._000079.pool.root.1`


The data is extracted from the Ntuple using the `uproot` package, and the jet $p_T$, $\eta$, JVT, and electron and muon overlap removal cuts are applied.

This mode just selects the same tracks that are used by the ip3d alg, sorts them 
by descending $s_{d0}$ (although other sorts can be specified by the `--sort_flag`), 
and truncates / pads the sequence to a fixed length of tracks specified by the --nTrks flag (default 15).

Jet variables are saved to a pandas Dataframe in a .h5 file, and the track variables
are saved to an xarray in a .nc file.


2. From previously processed .h5 and .nc files

Since option 1 is a sequential loop over the jets, for processing a lot of jets, 
it's best to parallelize and then just concatenate the files. The concatenation
step can also use this `root_to_np` function, and passing either the path to the 
.h5 or .nc files with the filename argument.


3. From the [training-dataset-dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) h5 files.

These ... are starting from

`python root_to_np.py --nJets 1000 --jetCollection PFLow --filename /eos/atlas/atlascerngroupdisk/perf-flavtag/training/dl1/PFlow/MC16d/MC16d_ttbar/user.mguth.18751537._000001.output.h5`

When we start from these files, the tracks will already have had a sort applied, 
and they've already had the relevant track cuts applied, and been truncated / padded 
to a constant number of tracks.

Since these track selection steps are taken care of in the training-dataset-dumper, 
it's up to the user to do the bookkeeping correctly to make sure that the flags 
for root_to_np.py are the same so that the organization in this repo is consistent. 

### ML preprocessing

Regardless of which of the above three options you started from, the rest of the script proceeds the same way.

- $p_T$ reweighting

    Since we don't want the network to learn to discrimination just based on the $p_T$
    distribution of the jets, so the `pTReweight` function uses the (uncalibrated) 
    jet $p_T$ to reweight $b$ and $c$-jet spectra to the $l$-jet distribution.

    However, if you want to use a different jet flavor as the reference distribution, 
    this can be done with the `--rwtDist` flag. (You'll also need to pass this flag 
    when running trainNet.py to load in the correct dataset.)

- Feature normalization
 
    The normalization options for the track features are determined by the flags 
    `--noNormVars`, `--logNormVars`, `--jointNormVars`. Each of these flags accepts 
    a comma separated list for the corresponding track features. The features in
    noNormVars will not be normalized, the ones in logNormVars will first be transformed
    by the natural log function, and then will be normalized, and those in jointNormVars
    will just be normalized as usual.

    The mean and standard deviation of the result of this scaling operation is 
    saved in a .json file to the same output directory that the other files created by 
    this script are saved to.

- Train and test dataset details

    There are three options for the output for running this script specified by the
    `--mode` flag. Either you can just make the training file `--mode train`, you
    can just make the test file `--mode test`, or you can do both with `--mode all` (the default) option.
    When running in the `--mode all` mode

    The option for making the training and test sets separately are due to the limited memory
    avaliable to store all the numpy arrays in memory when executing these functions.

    If you're running test mode where there are a different number of jets in the
    training and test sets, you'll need to specify which scaling file to use using
    the `--scalingFile`. The `--scale_tag` also lets you append an extra flag to the 
    data to keep track of which scaling was used when naming the files if desired.
    
    Note that if non-default `--mode` or `--scale_tag` flags are used, the same flags
    will also need to be used for running trainNet.py for the network training as well.
    

## Step 3: Training networks

`python trainNet.py --help`

The parameter `nEpochs` lets you set the maximum # of epochs for training, but you don't need to use the maximum, because the models are trained using early stopping, i.e, if the validation loss does not improve after 25 epochs, the training is stopped early.

This script supports two diffent models, RNNIP and DIPS, with argument flags for training with the different hyperparameters.

For the extended hybrid PFLow training, 400 hidden units for the RNN were used. 
The default hyperparameters provided for DIPS have been giving good performance for the $t\bar{t}$ R&D studies thus far.



## Step 4: Making plots

An example plotting script is provided in the `Notebooks/RetrainingStudies` folder which shows how to load in test sets 
and make both the roc curves and the $p_T$ and $\eta$ performance plots. 
Additional plotting functions are provided in the `plottingFcts.py` module.


### Additional information

This rnnip repo is a streamlined version of [RNNIP](https://gitlab.cern.ch/hartman/RNNIP), which includes additional preprocessing functionality,
model flexibility, and more R&D studies.



