import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, "../../PyATLASstyle/")
import PyATLASstyle as pas
pas.applyATLASstyle(mpl)
mpl.rcParams['legend.handlelength'] = 2.0
mpl.rcParams['lines.linewidth'] = 2.0

def compare_bl(b_arrs, l_arrs, cols, legs, label, binning, outfile, xlabel, upylims, doylims, uplogy=False, dology=False, lfsize=10):
    
    nbins, binmax, binmin = binning
    
    fig, (ax0,ax1) = plt.subplots(ncols=1, nrows=2, sharex=True, figsize=(8,8), gridspec_kw={'height_ratios': (3,1), 'hspace': 0.1} )
    
    for barr,larr,col,leg in zip(b_arrs, l_arrs, cols, legs):
        hb = ax0.hist( barr['h'], histtype='step', density=1, bins=nbins, range=(binmax, binmin), color=col, 
                 weights=barr['w'], label=leg+', b jets' )
        hl = ax0.hist( larr['h'], histtype='step', density=1, bins=nbins, range=(binmax, binmin), color=col, linestyle='dashed',
                 weights=barr['w'], label=leg+', l jets' )
        
        br = 0.5*(hb[1][:-1] + hb[1][1:])
        be = 0.5*(hb[1][1:] - hb[1][:-1])
        ra = np.divide( hb[0], hl[0], where=hb[0]>0, out=np.ones_like(hb[0]) )
        
        ax1.errorbar( x=br, y=ra, xerr=be, yerr=0, color=col, fmt='o', mfc='none', markersize=7.5)
        
    ax0.set_ylim( upylims[0], upylims[1] )
    ax1.set_ylim( doylims[0], doylims[1] )
    
    ax0.set_xlim( binmax, binmin )
    ax1.set_xlim( binmax, binmin )
    
    ax1.set_xlabel(xlabel, horizontalalignment='right', x=1.0)
    ax0.set_ylabel('Norm. Entries', horizontalalignment='right', y=1.0)
    ax1.set_ylabel('b jets/l jets')
    
    if uplogy: ax0.set_yscale('log')
    if dology: ax1.set_yscale('log')
        
    ax0.legend(ncol=1, frameon=False, fontsize=lfsize, bbox_to_anchor=(0.995, 0.97))
    
    pas.makeATLAStag(ax0, fig, first_tag='Simulation Internal', second_tag=label, ymax=0.9, line_spacing = 1.3, fsize=13.5)
    
    fig.savefig(outfile)
    plt.show()

    
        
        
        