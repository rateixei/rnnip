import matplotlib.pyplot as plt

from keras import backend as K
from keras.models import Model, load_model
from keras.layers import Layer, Masking, Input, Dense, Dropout, LSTM, concatenate
from keras.layers import LSTMCell, GRU, GRUCell, Bidirectional, RNN, ReLU, BatchNormalization
from keras.layers import BatchNormalization, Embedding, Lambda, TimeDistributed
from keras.layers import MaxPooling1D, AveragePooling1D, GlobalMaxPooling1D, GlobalAveragePooling1D
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.utils.np_utils import to_categorical
from keras.optimizers import Adam

import tensorflow as tf

class MeanPool(Layer):
    """
    Simple sum layer.
    The tricky bits are getting masking to work properly, but given
    that time distributed dense layers _should_ compute masking on their
    own.

    Author: Dan Guest
    https://github.com/dguest/flow-network/blob/master/SumLayer.py

    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

    def build(self, input_shape):
        pass

    def call(self, x, mask=None):
        if mask is not None:
            x = x * K.cast(mask, K.dtype(x))[:,:,None]
        return K.sum(x, axis=1) / K.sum( K.cast(mask, K.dtype(x))[:,:,None] )

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]

    def compute_mask(self, inputs, mask):
        return None
    
class MaxPool(Layer):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

    def build(self, input_shape):
        pass

    def call(self, x, mask=None):
        if mask is not None:
            x = x * K.cast(mask, K.dtype(x))[:,:,None]
        return K.max(x, axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]

    def compute_mask(self, inputs, mask):
        return None
    

class Sum(Layer):
    """
    Simple sum layer.
    The tricky bits are getting masking to work properly, but given
    that time distributed dense layers _should_ compute masking on their
    own.

    Author: Dan Guest
    https://github.com/dguest/flow-network/blob/master/SumLayer.py

    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True

    def build(self, input_shape):
        pass

    def call(self, x, mask=None):
        if mask is not None:
            x = x * K.cast(mask, K.dtype(x))[:,:,None]
        return K.sum(x, axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]

    def compute_mask(self, inputs, mask):
        return None



def make_dips_model(ntrkinputs, phi_layers=[100], 
                    f_layers=[100], do_jet_inputs=True, 
                    njetinputs=0, concat_op='sum'):
    
    if do_jet_inputs: 
        jet_inputs = Input(shape=(njetinputs,), name="jet_inputs")

    track_inputs = Input(shape=(ntrkinputs[0], ntrkinputs[1],), name="track_inputs")

    masked = Masking( mask_value=-99, name="masking_1")(track_inputs)

    tdd = 0
    
    for ipl,pl in enumerate(phi_layers):
        if ipl == 0:
            tdd = TimeDistributed(Dense(pl,activation='relu'),name=f"Phi{ipl}_Dense")(masked)
        else:
            tdd = TimeDistributed(Dense(pl,activation='relu'),name=f"Phi{ipl}_Dense")(tdd)

    if concat_op == 'sum':
        F = Sum()(tdd)
    elif concat_op == 'max':
        F = MaxPool()(tdd)
    elif concat_op == 'mean':
        F = MeanPool()(tdd)
    else:
        print(f"Couldn't find {concat_op} in options for concatenation layer")
        return -1


    if do_jet_inputs: 
        F_batch = BatchNormalization(axis=1)(F)
        jet_inputs_batch = BatchNormalization(axis=1)(jet_inputs)
        hidden = concatenate([F_batch,jet_inputs_batch])
    else:
        hidden = F

    for ifl,fl in enumerate(f_layers):
        hidden = Dense(fl,activation='relu')(hidden)

    main_output = Dense(1, activation='sigmoid', name="main_output")(hidden)

    if do_jet_inputs:
        model = Model(inputs=[jet_inputs,track_inputs], outputs=[main_output])
    else:
        model = Model(inputs=[track_inputs], outputs=[main_output])
        
    model.summary()
    optAdam = Adam(lr=0.001)
    model.compile(loss='binary_crossentropy', optimizer=optAdam, metrics=["accuracy"])
    
    return model

def train_model(model, X_training_total, y_train, w_train, 
                batch_size, nb_epoch, out_weight, patience,
                save_checkpoint=False):
    
    # set learning phase for no training
    K.set_learning_phase(1)
    
    history = model.fit( X_training_total , y_train, 
                    batch_size=batch_size, 
                    epochs=nb_epoch, 
                    validation_split=0.1, 
                    shuffle = True,
                    sample_weight= w_train,
                    callbacks = [
                        EarlyStopping(verbose=True, patience=patience, monitor='val_loss'),
                        ModelCheckpoint(out_weight, monitor='val_loss', verbose=True, save_best_only=True)
                    ],
    )
    
    fig, (ax1,ax2) = plt.subplots(ncols=2, figsize=(15,5))
    for kk in history.history:
        if 'loss' in kk:
            ax1.plot(history.history[kk])
        else:
            ax2.plot(history.history[kk])
    ax1.set_xlabel("training epochs")
    ax1.legend(["Validation", "Training"])
    ax1.set_ylabel("Loss")
    ax2.set_xlabel("training epochs")
    ax2.legend(["Validation", "Training"])
    ax2.set_ylabel("Accuracy")
    plt.savefig("plots/" + out_weight.strip('/')[-1].replace(".h5", "_history.pdf") )
    plt.show()    
    
    print("Saving json model to:", out_weight.replace(".h5", "_arch.json" ) )
    
    with open( out_weight.replace(".h5", "_arch.json" ), "w") as f:
        f.write(model.to_json())
    
    print("Saving full model to:",  out_weight.replace(".h5", "_full_model.h5" ))
    model.save( out_weight.replace(".h5", "_full_model.h5" ) )
    
    model.load_weights(out_weight)
    
    if save_checkpoint:
        # set learning phase for no training
        K.set_learning_phase(0)

        # make list of output node names
        output_names=[out.op.name for out in model.outputs]

        # set up tensorflow saver object
        saver = tf.train.Saver()

        # fetch the tensorflow session using the Keras backend
        tf_session = K.get_session()

        # get the tensorflow session graph
        input_graph_def = tf_session.graph.as_graph_def()

        # write out tensorflow checkpoint & inference graph for use with freeze_graph script
        save_path = saver.save(tf_session, './'+ out_weight.replace(".h5", "tf_chkpt.ckpt" ) )
        
        out_weight_dir = '.' + '/'.join(out_weight.split('/')[:-1]) + '/'
        
        tf.train.write_graph(input_graph_def, out_weight_dir, out_weight.split('/')[-1].replace('.h5', '_tf_infer_graph.pb'), as_text=False)
    
    
    return history
    
