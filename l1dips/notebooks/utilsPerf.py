import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.interpolate import pchip

import sys
sys.path.insert(0, "../../PyATLASstyle/")
import PyATLASstyle as pas
pas.applyATLASstyle(mpl)
mpl.rcParams['legend.handlelength'] = 2.0
mpl.rcParams['lines.linewidth'] = 2.0

def get_eff_from_hist(hhh):
    return ((np.add.accumulate( hhh[0][::-1] ) ) / np.sum(hhh[0]))

def make_roc_curve(score_sig, score_bkg, xmin, xmax):
    sig_h = np.histogram(score_sig, bins=500, range=(xmin, xmax))
    bkg_h = np.histogram(score_bkg, bins=500, range=(xmin, xmax))
    
    sig_eff = get_eff_from_hist(sig_h)
    bkg_rej = np.divide(np.ones_like(get_eff_from_hist(bkg_h)), get_eff_from_hist(bkg_h), where=get_eff_from_hist(bkg_h)>0, out=np.ones_like(get_eff_from_hist(bkg_h)))
    
    return (sig_eff, bkg_rej)

def find_wp(score_sig, effwp, xmin, xmax):
    sig_h = np.histogram(score_sig, bins=5000, range=(xmin, xmax))
    sig_eff = get_eff_from_hist(sig_h)[::-1]
    abs_val_eff_diff = np.abs(sig_eff - effwp*np.ones_like(sig_eff) )
    arg_min = np.argmin(abs_val_eff_diff)
    
#     print("Found efficiency",effwp, "in bin", arg_min, "corresponding to", sig_eff[arg_min], "and x axis", sig_h[1][arg_min] )
    
    return sig_h[1][arg_min]

def make_roc_comparison(outfile, rocs, label, xlabel, upylabel, doylabel, xmin, xmax, upymin, upymax, doymin, doymax, doylog=False, is_prem=False, lfsize=10):
    
    fig, (ax0,ax1) = plt.subplots(ncols=1, nrows=2, sharex=True, figsize=(8,8), gridspec_kw={'height_ratios': (3,1), 'hspace': 0.1} )
    
    my_inters = []
    for irr,rr in enumerate(rocs):
        col = rr[4]
        stl = '-'
        if 'IP3D' in rr[2]: stl = '--'
        ax0.semilogy( rr[0], rr[1], label=rr[2], linestyle=stl, color=col)
        unique_x, unique_indx = np.unique(rr[0], return_index=True)
        unique_y = rr[1][unique_indx]
        roc_inter_func = pchip(unique_x, unique_y)
        my_inters.append(roc_inter_func)
        ratio_to = rr[3]
        if ratio_to >= 0:
            x_inter = np.linspace(xmin, xmax, 500)
            this_inter_roc = roc_inter_func(x_inter)
            first_inter_roc = my_inters[ratio_to](x_inter)
            this_ratio = np.divide(this_inter_roc, first_inter_roc)
            ax1.plot(x_inter, this_ratio, linestyle=stl, color=col)
            
    ax0.set_xlim(xmin, xmax)
    ax1.set_xlim(xmin, xmax)
    ax0.legend(ncol=1, frameon=False, fontsize=lfsize, bbox_to_anchor=(0.995, 0.97))
    ax0.set_ylim(upymin, upymax)
    ax1.set_ylim(doymin, doymax)
    
    ax0.set_ylabel(upylabel, horizontalalignment='right', y=1.0)
    ax1.set_ylabel(doylabel)
    ax1.set_xlabel(xlabel, horizontalalignment='right', x=1.0)
    
    if doylog:
        ax1.set_yscale('log')
    
    first_tag = "Simulation Internal" if is_prem == False else "Simulation Preliminary"
    pas.makeATLAStag(ax0, fig, first_tag=first_tag, second_tag=label, ymax=0.9, line_spacing = 1.3, fsize=13.5)
    
    fig.savefig(outfile)
    plt.show()

def geteff(vararr, scorearr, scorecut, bins):
    eff_err = lambda x, N: np.sqrt( x*(1-x) / N)
    hall,ball = np.histogram(vararr, bins=bins)
    hcut,bcut = np.histogram(vararr[scorearr > scorecut], bins=bins)
    heff = np.divide( 1.0*hcut,1.0*hall, where=hall>0, out=np.zeros_like(hall, dtype='d') )
#     print(hcut,hall,heff)
    herr = eff_err(heff, hall)
    hbins = 0.5*( ball[:-1] + ball[1:] )
    xerr = 0.5*( ball[1:] - ball[:-1] )
    return heff,herr,hbins,xerr

def make_eff_comparison(outfile, effs, label, xlabel, upylabel, doylabel, xmin, xmax, upymin, upymax, doymin, doymax, dolog=False, dologlow=False, legloc='upper right', is_prem=False):
    
    fig, (ax0,ax1) = plt.subplots(ncols=1, nrows=2, sharex=True, figsize=(8,8), gridspec_kw={'height_ratios': (3,1), 'hspace': 0.1} )
    
    for irr,rr in enumerate(effs):
        col = rr[6]
        to_ratio = rr[5]
        marker_style = 's'
        if "IP3D" in rr[4]:
            marker_style = 'D'
        ax0.errorbar( x=rr[0], y=rr[1], xerr=rr[2], yerr=0, label=rr[4], color=col, fmt=marker_style, mfc='none', markersize=7.5)

        if to_ratio >= 0:
            my_ratio = []
            my_ratio_err = []
            min_len = min( len(rr[0]), len(effs[to_ratio][0]) )
            for ib in range(min_len):
                if ib >= len(effs[to_ratio][0]):
                    break
                else:
                    rat_eff = rr[1][ib]/effs[to_ratio][1][ib]
                    rat_eff_err = rr[3][ib]/effs[to_ratio][1][ib]
                    my_ratio.append(rat_eff)
                    my_ratio_err.append(rat_eff_err)
            ax1.errorbar(x=rr[0][0:min_len], y=my_ratio[0:min_len], xerr=rr[2][0:min_len], yerr=0, color=col, mfc='none', fmt=marker_style, markersize=7.5)
            
    ax0.set_xlim(xmin, xmax)
    ax1.set_xlim(xmin, xmax)
    ax0.legend(ncol=1, frameon=False, loc=legloc)
#     ax0.legend(ncol=1, frameon=False, loc='upper right')
    ax0.set_ylim(upymin, upymax)
    ax1.set_ylim(doymin, doymax)
    
    ax0.set_ylabel(upylabel, horizontalalignment='right', y=1.0)
    ax1.set_ylabel(doylabel)
    ax1.set_xlabel(xlabel, horizontalalignment='right', x=1.0)
    
    if dolog:
        ax0.set_yscale('log')
    if dologlow:
        ax1.set_yscale('log')
    
    first_tag = "Simulation Internal" if is_prem == False else "Simulation Preliminary"
    pas.makeATLAStag(ax0, fig, first_tag=first_tag, second_tag=label, ymax=0.9, line_spacing = 1.3)
    
    fig.savefig(outfile)
    plt.show()