import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import h5py
import uproot_methods

def get_eff_from_hist(hhh):
    return ((np.add.accumulate( hhh[0][::-1] ) ) / np.sum(hhh[0]))

def open_dataset(filename):
    dataset = {}
    file_path = filename
    with h5py.File(file_path, 'r') as hf:
        for key in hf.keys():
            dataset[key] = hf[key][()]
            print(key)
    return dataset

def save_important_stuff(outfile, pt_arr, eta_arr, ip3d_arr, dips_arr, flav_arr):
    with h5py.File(outfile, "w") as outf:
        outf.create_dataset("pt", data=pt_arr)
        outf.create_dataset("eta", data=eta_arr)
        outf.create_dataset("ip3d", data=ip3d_arr)
        outf.create_dataset("dips", data=dips_arr)
        outf.create_dataset("flav", data=flav_arr)

def filter_b_or_light(dataset, data_matrix):
    
    b_or_light_filter = (dataset["jet_flav"]==5)|(dataset["jet_flav"]==0)
    data_matrix_filtered = data_matrix[ b_or_light_filter ]
    data_flavor_filtered = dataset["jet_flav"][ b_or_light_filter ]
    data_weight_filtered = dataset["jet_weights"][ b_or_light_filter ]
    data_y = 1.0*( data_flavor_filtered==5 )
    
    return b_or_light_filter, data_matrix_filtered, data_flavor_filtered, data_weight_filtered, data_y

# def calculate_trkvec_pt_dr(dataset):
#     jets_vecs = uproot_methods.TLorentzVectorArray.from_ptetaphie(
#         dataset['jet_pt'],
#         dataset['jet_eta'],
#         dataset['jet_phi'],
#         dataset['jet_E']
#     )
#     vtrksums_pt_frac = []
#     vtrksums_dr = []
#     n_jets = len(dataset["jet_pt"])
#     for ijet in range(n_jets):
#         vtrks = uproot_methods.TLorentzVectorArray.from_ptetaphim(
#             dataset['trk_pt'][ijet],
#             dataset['trk_eta'][ijet],
#             dataset['trk_phi'][ijet],
#             139.57*np.ones_like(dataset['trk_phi'][ijet])
#         )
#         vtrks = vtrks.sum()
#         vtrksums_pt_frac.append( np.log(vtrks.pt/jets_vecs[ijet].pt) if vtrks.pt > 0 else np.nan )
#         vtrksums_dr.append( np.log(vtrks.delta_r(jets_vecs[ijet])) if vtrks.pt > 0 else np.nan )
    
    
#     return np.array(vtrksums_pt_frac), np.array(vtrksums_dr)
    

def make_jet_matrix(dataset, jet_inputs, b_or_light_filter, do_trkvec_vars=False):
    tot_inputs = len(jet_inputs)
#     if do_trkvec_vars:
#         tot_inputs += 2
    X_jets = np.ones((dataset[jet_inputs[0]].shape[0],tot_inputs))
    
    for ij,jet_inp in enumerate(jet_inputs):
        if "_eta" in jet_inp:
            X_jets[:,ij] = np.abs(dataset[jet_inp])
        else:
            X_jets[:,ij] = dataset[jet_inp]

#     if do_trkvec_vars:
#         trkvec_vars = calculate_trkvec_pt_dr(dataset)
#         X_jets[:,-2] = trkvec_vars[0]
#         X_jets[:,-1] = trkvec_vars[1]

    for iji in range(X_jets.shape[1]):
        isnan = np.isnan(X_jets[:,iji])
        iji_mean = np.nanmean(X_jets[:,iji],axis=0)
        iji_std = np.nanstd(X_jets[:,iji],axis=0)
        X_jets[:,iji] = (X_jets[:,iji] - iji_mean)/iji_std
        X_jets[:,iji][isnan] = -99
    
    X_jets_filtered = X_jets[ b_or_light_filter, : ]
    
    return X_jets, X_jets_filtered


def make_data_matrix(ds, 
                     track_vars_to_train,
                     log_transform_list=None,
                     max_tracks=20,
                     max_dr=50):
    
    n_tracks_in_jets = ds["jet_ntrks"]
    max_num_tracks = min( n_tracks_in_jets.max(), max_tracks)
    print("Max number of tracks per jet to train with", max_num_tracks)
    
    n_jets = len(ds["jet_pt"])
    print("Total number of jets", n_jets)
    
    n_vars = len(track_vars_to_train)
    
    #use NaN, since there are handy np.nanmean and np.nanstd functions to ignore nan
    out_array = np.full( (n_jets, max_num_tracks, n_vars),
                        np.nan,
                        dtype=float )
    
    for ivar,var in enumerate(track_vars_to_train):
        to_log = True if log_transform_list is not None and var in log_transform_list else False
        for ijet in range(n_jets):

            trks_to_save = min(n_tracks_in_jets[ijet],max_num_tracks)
            
            trks_dr = ds['trk_dr'][ijet][:trks_to_save] < max_dr
            
            trks_pt = ds['trk_pt'][ijet][:trks_to_save] > 4000.

            this_arr_to_save = ds[var][ijet][:trks_to_save][trks_dr & trks_pt]
            if to_log:
                this_arr_to_save = np.log(this_arr_to_save)
            n_trks_to_save = this_arr_to_save.shape[0]
            out_array[ijet,:n_trks_to_save,ivar] = this_arr_to_save
            
    #precompute where NaN's are left, so we can norm everything, then reset what was NaN to 0
    isnan = np.isnan(out_array)
    mean = None
    std = None
    
    mean = np.nanmean(out_array, axis=0)
    std  = np.nanstd( out_array, axis=0)
    
    #fix cases when std==0, don't norm
    mean[std==0] = 0.
    std[std==0] = 1.
        
    out_array = (out_array - mean)/std
    
    #clean up NaN's 
    out_array[isnan] = -99
    
    out_array_ordered = np.ones_like(out_array)
    
    for idm,dm in enumerate(out_array):
        sig_d0s = dm[:,2]
        sorted_indices = np.argsort(sig_d0s)[::-1]
        out_array_ordered[idm,:,:] = dm[sorted_indices][:,]
    
    return out_array_ordered, mean, std