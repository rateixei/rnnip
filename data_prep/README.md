# Data preparation for L1Track b-tag datasets

## Setup environment

```
singularity exec -B /gpfs docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-cpu/ml-cpu:latest bash
```

## Run on a single file

```
python make_data.py --input user.cantel.19806744.BTAGSTREAM._000001.root
```