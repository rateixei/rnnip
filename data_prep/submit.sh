#!/bin/bash


SCR=/gpfs/slac/atlas/fs1/u/rafaeltl/L1BTagATLAS/training/rnnip/data_prep/make_data.py

DATALOC=/gpfs/slac/atlas/fs1/d/rafaeltl/public/L1BTag/
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11004_RT_ConfigHTT_eff95flat_NewMoU_InIn_sf2_pt4_v1_BTAGSTREAM/user.cantel.20664733.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11004_RT_ConfigHTT_eff95flat_NewMoU_OutOut_sf2_pt4_v1_BTAGSTREAM/user.cantel.20662068.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11004_RT_ConfigHTT_eff95flat_MoU2_sf2_pt4_v2_BTAGSTREAM/user.cantel.20724728.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11002_RT_ConfigHTT_eff95flat_TDR2in_sf2_pt4_v1_BTAGSTREAM/user.cantel.20792264.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11002_ConfigHTT_eff95flat_NoPix_In_sf2_pt4_v1_BTAGSTREAM/user.cantel.21031914.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11002_ConfigHTT_eff95flat_MoU_L1_InIn_sf2_pt4_v1_BTAGSTREAM/user.cantel.21072958.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11002_eff95flat_PIX4L2_InIn_sf2_pt4_v1_BTAGSTREAM/user.cantel.21081176.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11004_RT_ConfigHTT_eff100flat_v1_BTAGSTREAM/user.cantel.19589229.BTAGSTREAM
# AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11002_MOU2in_L1TrkSimBasedWithOffPtDep_wFake_BTAGSTREAM/user.cantel.22242266.BTAGSTREAM
AOD=user.cantel.mc15_14TeV.117050.PowhegPythia_P2011C_ttbar_r11002NOPIX_L1TrkSimBasedWithOffPtDep_wFake_BTAGSTREAM/user.cantel.22235127.BTAGSTREAM

H5LOC=${DATALOC}/ml_datasets/
# H5NAME=ttbar_r11004_RT_ConfigHTT_eff95flat_NewMoU_InIn_sf2_pt4_v1_BTAGSTREAM
# H5NAME=ttbar_r11004_RT_ConfigHTT_eff95flat_NewMoU_OutOut_sf2_pt4_v1_BTAGSTREAM
# H5NAME=ttbar_r11004_RT_ConfigHTT_eff95flat_MoU2_sf2_pt4_v2_BTAGSTREAM
# H5NAME=ttbar_r11002_RT_ConfigHTT_eff95flat_TDR2in_sf2_pt4_v1_BTAGSTREAM
# H5NAME=ttbar_r11002_ConfigHTT_eff95flat_NoPix_In_sf2_pt4_v1_BTAGSTREAM
# H5NAME=ttbar_r11002_ConfigHTT_eff95flat_MoU_L1_InIn_sf2_pt4_v1_BTAGSTREAM
# H5NAME=ttbar_r11002_eff95flat_PIX4L2_InIn_sf2_pt4_v1_BTAGSTREAM
# H5NAME=ttbar_r11004_RT_ConfigHTT_eff100flat_v1_BTAGSTREAM
# H5NAME=ttbar_r11002_MOU2in_L1TrkSimBasedWithOffPtDep_wFake_BTAGSTREAM
H5NAME=ttbar_r11002NOPIX_L1TrkSimBasedWithOffPtDep_wFake_BTAGSTREAM


# bsub -q atl-analq -R select[!kiso] -W 50:00 " python ${SCR} -i '${DATALOC}/${AOD}._00*.root' -o ${H5LOC}/${H5NAME}_gathered.h5 "

# exit 1

for i in  00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 ;
do
    echo $i;
    bsub -q atl-analq -R select[!kiso] -W 50:00 " python ${SCR} -i '${DATALOC}/${AOD}._00${i}*.root' -o ${H5LOC}/${H5NAME}_${i}.h5 " ;
done
