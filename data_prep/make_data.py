import numpy as np
import uproot
import argparse
import filetools as ftl
import h5py
import glob 
import uproot_methods

DEBUG=False

def deltaeta(eta1, eta2):
    """Return the pseudorapidity difference, :math:`\\Delta \\eta`, with another Lorentz vector."""
    return eta1 - eta2

def deltaphi(phi1, phi2):
    """Return the phi angle difference, :math:`\\Delta \\phi`, with another Lorentz vector."""
    dphi = phi1 - phi2
    while dphi > np.pi:
        dphi -= 2*np.pi
    while dphi < -np.pi:
        dphi += 2*np.pi
    return dphi

def deltar(p1, p2):
    """Return :math:`\\Delta R` the distance in (eta,phi) space with another Lorentz vector, defined as:
    :math:`\\Delta R = \\sqrt{(\\Delta \\eta)^2 + (\\Delta \\phi)^2}`
    """
    return np.sqrt( deltaeta( p1[0], p2[0] )**2 + deltaphi(p1[1], p2[1])**2 )

class jet:
    def __init__(self):
        self.jet_data = {
            'jet_pt': -99,
            'jet_eta': -99,
            'jet_phi': -99,
            'jet_E': -99,
            'jet_flav': -99,
            'jet_ntrks': -99,
            'jet_ip3d_llr': -99,
            'jet_tvec_dr': -99,
            'jet_tvec_ptfrac': -99,
            'trk_pt': [],
            'trk_eta': [],
            'trk_phi': [],
            'trk_chi2': [],
            'trk_dr': [],
            'trk_ptfrac': [],
            'trk_ip3d_d0': [],
            'trk_ip3d_z0': [],
            'trk_ip3d_sig_d0': [],
            'trk_ip3d_sig_z0': [],
            # 'trk_nNextToInnHits': [], 
            # 'trk_nInnHits': [], 
            # 'trk_nsharedBLHits': [], 
            # 'trk_nsplitBLHits': [], 
            # 'trk_nsharedPixHits': [], 
            # 'trk_nsplitPixHits': [], 
            # 'trk_nsharedSCTHits': [], 
            # 'trk_nPixHits': [], 
            # 'trk_nSCTHits': []
        }

    def jet_dict(self):
        return self.jet_data

    def make_jet(self, data, iet, ijt):

        if data[b'AK4EM_jet_pt'][iet][ijt] < 15e3: return False
        if np.abs(data[b'AK4EM_jet_eta'][iet][ijt]) > 4: return False

        self.jet_data['jet_pt'] = data[b'AK4EM_jet_pt'][iet][ijt]
        self.jet_data['jet_eta'] = data[b'AK4EM_jet_eta'][iet][ijt]
        self.jet_data['jet_phi'] = data[b'AK4EM_jet_phi'][iet][ijt]
        self.jet_data['jet_E'] = data[b'AK4EM_jet_E'][iet][ijt]
        self.jet_data['jet_ip3d_llr'] = data[b'AK4EM_IP3D_jet_ipxd_llr'][iet][ijt]

        if DEBUG:
            print("This jet:")
            print("\t jet_pt ", self.jet_data['jet_pt'])
            print("\t jet_eta ", self.jet_data['jet_eta'])
            print("\t jet_phi ", self.jet_data['jet_phi'])
            print("\t jet_E ", self.jet_data['jet_E'])

        
        #truth quality requirement
        if data[b'AK4EM_jet_truthMatch'][iet][ijt] and \
          data[b'AK4EM_jet_quarkMatch'][iet][ijt] and \
          np.abs( data[b'HSz'][iet] - data[b'PVz'][iet] ) < 0.1:

            jet_flav = 0
            if data[b'AK4EM_jet_truthflavExCHad'][iet][ijt] == 5:
                jet_flav = 5
            elif data[b'AK4EM_jet_truthflavExCHad'][iet][ijt] == 4:
                jet_flav = 4
            else:
                jet_flav = 0

        else:
            return False

        self.jet_data['jet_flav'] = jet_flav
        trks = data[b'AK4EM_IP3D_jet_ipxd_trackAssoc_index'][iet][ijt]

        if DEBUG: print("\t AK4EM_IP3D_jet_ipxd_trackAssoc_index", trks)

        self.jet_data['jet_ntrks'] = len(trks)

        #Sd0 ordering
        trksdz0s = [ [data[b'AK4EM_IP3D_jet_ipxd_sigD0wrtPVofTracks'][iet][ijt][itrk], indtrk] for itrk,indtrk in enumerate(trks) ]

        trksdz0s.sort( key= lambda x: x[0], reverse=True )

        trkvec = uproot_methods.TLorentzVector().from_ptetaphie(0,0,0,0)

        for itrk,itrkd0 in enumerate(trksdz0s):
            if itrk > 20: break

            trkvec_id = uproot_methods.TLorentzVector.from_ptetaphim(data[b'trk_pt'][iet][trk_id],
                                                                       data[b'trk_eta'][iet][trk_id],
                                                                       data[b'trk_phi'][iet][trk_id],
                                                                       139.57 ) #pion mass in MeV
            trkvec = trkvec + trkvec_id

            trk_id = itrkd0[1]

            if DEBUG:
                print("\t\t Track ID", itrkd0[1] )
                print("\t\t Track pt", data[b'trk_pt'][iet][trk_id] )
                print("\t\t Track eta", data[b'trk_eta'][iet][trk_id] )
                print("\t\t Track phi", data[b'trk_phi'][iet][trk_id] )
            
            self.jet_data['trk_pt'].append( data[b'trk_pt'][iet][trk_id] )
            self.jet_data['trk_eta'].append( data[b'trk_eta'][iet][trk_id] )
            self.jet_data['trk_phi'].append( data[b'trk_phi'][iet][trk_id] )
            self.jet_data['trk_chi2'].append( data[b'trk_chi2'][iet][trk_id] )
            self.jet_data['trk_ptfrac'].append( data[b'trk_pt'][iet][trk_id]/self.jet_data['jet_pt'] )
            self.jet_data['trk_ip3d_d0'].append( data[b'AK4EM_IP3D_jet_ipxd_valD0wrtPVofTracks'][iet][ijt][itrk] )
            self.jet_data['trk_ip3d_z0'].append( data[b'AK4EM_IP3D_jet_ipxd_valZ0wrtPVofTracks'][iet][ijt][itrk] )
            self.jet_data['trk_ip3d_sig_d0'].append( data[b'AK4EM_IP3D_jet_ipxd_sigD0wrtPVofTracks'][iet][ijt][itrk] )
            self.jet_data['trk_ip3d_sig_z0'].append( data[b'AK4EM_IP3D_jet_ipxd_sigZ0wrtPVofTracks'][iet][ijt][itrk] )
            # self.jet_data['trk_nInnHits'].append( data[b'trk_nInnermostPixelLayerHits'][iet][trk_id])
            # self.jet_data['trk_nsharedBLHits'].append( data[b'trk_nInnermostPixelLayerSharedHits'][iet][trk_id])
            # self.jet_data['trk_nsplitBLHits'].append( data[b'trk_nInnermostPixelLayerSplitHits'][iet][trk_id])
            # self.jet_data['trk_nNextToInnHits'].append( data[b'trk_nNextToInnermostPixelLayerHits'][iet][trk_id])
            # self.jet_data['trk_nsharedPixHits'].append( data[b'trk_nsharedPixHits'][iet][trk_id])
            # self.jet_data['trk_nsplitPixHits'].append( data[b'trk_nsplitPixHits'][iet][trk_id])
            # self.jet_data['trk_nPixHits'].append( data[b'trk_nPixHits'][iet][trk_id])
            # self.jet_data['trk_nsharedSCTHits'].append( data[b'trk_nSCTSharedHits'][iet][trk_id])
            # self.jet_data['trk_nSCTHits'].append( data[b'trk_nSCTHits'][iet][itrk])
            trk_jet_dr = deltar(
                    [self.jet_data['jet_eta'], self.jet_data['jet_phi']],
                    [data[b'trk_eta'][iet][trk_id], data[b'trk_phi'][iet][trk_id]]
                    ) 
            if trk_jet_dr > 0.5:
                print("DR between track and jet is too big, something is wrong!")
                sys.exit()
            self.jet_data['trk_dr'].append(trk_jet_dr)

        self.jet_data['jet_tvec_ptfrac'] = trkvec.pt/self.jet_data['jet_pt']
        self.jet_data['jet_tvec_dr'] = deltar(
                    [self.jet_data['jet_eta'], self.jet_data['jet_phi']],
                    [trkvec.eta, trkvec.phi]
                    )
        return True

def concat_jets(jets):
    alljets = jet().jet_dict()
    for kk in alljets:
        if "jet" in kk:
            alljets[kk] = []
        for jj in jets:
            alljets[kk].append(jj[kk])
    return alljets
    

def loop(data, nentries):
    myjets = []
    for iet in range(nentries):
        if iet%100 == 0:
            print(f"Running on event {iet}")
        njets = len(data[b'AK4EM_jet_pt'][iet])
        for ijt in range(njets):
            myjet = jet()
            if myjet.make_jet(data, iet, ijt):
                myjets.append( myjet.jet_data )
    myjets_dict = concat_jets(myjets)
    if DEBUG: 
        print(myjets_dict)
    return myjets_dict
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Data processing')
    parser.add_argument('-i', '--input', dest='input', required=True, type=str, help='Input file')
    parser.add_argument('-o', '--output', dest='out', required=True, type=str, help='Output file')
    args = parser.parse_args()

    files = []
    if '*' in args.input:
        files = glob.glob(args.input)
    else:
        files.append(args.input)

    my_total_jets = []
    for iff,ff in enumerate(files):
        print(f"Reading file {iff} out of {len(files)}: {ff}")
        with uproot.open(ff) as f:
            t = f["bTag"].arrays(ftl.branches_to_read)
            my_jets = loop(t, f["bTag"].numentries)
            my_total_jets.append(my_jets)

    concated_dict = {}
    if len(my_total_jets) < 1:
        print("Couldnt find any jet, exiting...")
        sys.exit()
    elif len(my_total_jets) == 1:
        concated_dict = my_total_jets[0]
    else:
        for key,val in my_total_jets[0].items():
            concated_dict[key] = []
            for mtj in my_total_jets:
                concated_dict[key] += mtj[key]

    with h5py.File(args.out, 'w') as hf:
        dt = h5py.special_dtype(vlen=np.dtype('float32'))
        njets = len(concated_dict["jet_pt"])
        for key,val in concated_dict.items():
            if 'jet_' in key:
                hf.create_dataset(key, data=concated_dict[key])
            else:
                hf.create_dataset(key, (njets,), dtype=dt)
                hf[key][...] = val
        print("Saved!")


