import numpy as np
import h5py
import argparse
import bisect

parser = argparse.ArgumentParser(description='Data processing')
parser.add_argument('-i', '--input', dest='input', required=True, type=str, nargs='+', help='Input file')
parser.add_argument('-o', '--output', dest='out', required=True, type=str, help='Output file')
parser.add_argument('--plot', dest='plot', default=False, action='store_true', help='Make pt plot')
args = parser.parse_args()

print(args.input)

my_dict = {}

for iff,ff in enumerate(args.input):
    print("Reading", ff)
    if iff == 0:
        with h5py.File(ff, 'r') as f:
            for kk in f.keys():
                my_dict[kk] = np.array(f[kk])
    else:
        with h5py.File(ff, 'r') as f:
            for kk in f.keys():
                my_dict[kk] = np.concatenate( [my_dict[kk], np.array(f[kk])] )

for kk in my_dict:
    print(kk, my_dict[kk].shape)

my_bs = my_dict["jet_flav"] == 5
my_cs = my_dict["jet_flav"] == 4
my_ls = my_dict["jet_flav"] == 0

print(
    "Dataset content:", '\n' ,
    "\t b jets:", my_bs.sum(), '\n' ,
    "\t c jets:", my_cs.sum(), '\n' ,
    "\t l jets:", my_ls.sum()
)

#Doing pT reweighting

hist_b = np.histogram( my_dict["jet_pt"][my_bs], bins=200, range=(0, 500*1e3) )
hist_c = np.histogram( my_dict["jet_pt"][my_cs], bins=200, range=(0, 500*1e3) )
hist_l = np.histogram( my_dict["jet_pt"][my_ls], bins=200, range=(0, 500*1e3) )

lc_rat = np.divide( hist_l[0]/hist_l[0].sum(), hist_c[0]/hist_c[0].sum(), where=hist_c[0]>0, out=np.ones_like(hist_c[0], dtype='d') )
lb_rat = np.divide( hist_l[0]/hist_l[0].sum(), hist_b[0]/hist_b[0].sum(), where=hist_b[0]>0, out=np.ones_like(hist_b[0], dtype='d') )

def get_c_weights(thispt):
    pt_maxed = min( thispt, 499*1e3 )
    this_index = bisect.bisect_right(hist_l[1], pt_maxed) - 1
    return lc_rat[this_index]

def get_b_weights(thispt):
    pt_maxed = min( thispt, 499*1e3 )
    this_index = bisect.bisect_right(hist_l[1], pt_maxed) - 1
    return lb_rat[this_index]

get_c_weights_vec = np.vectorize(get_c_weights)
get_b_weights_vec = np.vectorize(get_b_weights)

my_dict["jet_weights"] = np.ones_like(my_dict["jet_pt"])
my_dict["jet_weights"][my_cs] = get_c_weights_vec( my_dict["jet_pt"][my_cs] )
my_dict["jet_weights"][my_bs] = get_b_weights_vec( my_dict["jet_pt"][my_bs] )

if args.plot:
    import matplotlib.pyplot as plt
    plt.Figure()
    plt.hist( my_dict["jet_pt"][my_bs], bins=200, range=(0, 100*1e3), histtype='step', color='black', density=1, label='b no weight' )
    plt.hist( my_dict["jet_pt"][my_cs], bins=200, range=(0, 100*1e3), histtype='step', color='red', density=1, label='c no weight' )
    plt.hist( my_dict["jet_pt"][my_ls], bins=200, range=(0, 100*1e3), histtype='step', color='green', density=1, label='l no weight' )
    plt.hist( my_dict["jet_pt"][my_cs], bins=200, range=(0, 100*1e3), histtype='step', color='red', density=1, label='c weighted', linestyle='dashed', weights=my_weights[my_cs] )
    plt.hist( my_dict["jet_pt"][my_bs], bins=200, range=(0, 100*1e3), histtype='step', color='green', density=1, label='b weighted', linestyle='dashed', weights=my_weights[my_bs] )
    plt.legend()
    plt.xlabel("Jet pT [MeV]")
    plt.yscale('log')
    plt.ylabel('Jets (norm.)')
    plt.savefig("jet_pt.png")

with h5py.File(args.out, 'w') as hf:
    dt = h5py.special_dtype(vlen=np.dtype('float32'))
    njets = len(my_dict["jet_pt"])
    for key,val in my_dict.items():
        if 'jet_' in key:
            hf.create_dataset(key, data=my_dict[key])
        else:
            hf.create_dataset(key, (njets,), dtype=dt)
            hf[key][...] = val
    print("Saved!")